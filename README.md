# Pizza-Sales

A simple app to predict pizza sales. Many different python libraries are used in the project. I used the linear regression method to predict.


**To use:

-Clone the project

-Go to project directory

-Open command prompt and type "pip install -r requirements.txt"

-Make sure you're in the same directory as app.py

-Type "flask run"

-Go to the flask url**
