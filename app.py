from flask import Flask,render_template,session,flash,redirect,url_for,request
from wtforms import Form,StringField,TextAreaField,PasswordField,validators,FileField,IntegerField,SelectField
from wtforms.fields.html5 import EmailField
from functools import wraps
import sqlite3
from werkzeug.utils import secure_filename
import datetime
import time
import codecs
import os
import io
import pandas as pd
import matplotlib.pyplot as plt
from pandas import DataFrame
import statsmodels.api as sm
from datetime import datetime

import numpy as np
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
from sklearn.linear_model import LinearRegression

#file_upload
UPLOAD_FOLDER = 'static/files/'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
EXTENSIONS = set(['xlsx', 'xls'])
app.secret_key= "pizzaapp"
#database

con = sqlite3.connect("pizza.db")
cursor = con.cursor()
cursor.execute("CREATE TABLE IF NOT EXISTS tasks (taskname TEXT,time_now DATETIME, started_date DATETIME, finished_date DATETIME,step NUMBER, file_url CHAR,username CHAR)")
cursor.execute("CREATE TABLE IF NOT EXISTS users (username TEXT, pass PASSWORD, email TEXT)")
con.commit() 
cursor.close()


def extension_control(filename):
       if '.' in filename and filename.rsplit('.', 1)[1].lower() in EXTENSIONS:
            return True
       else:
            return False

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
          if "logged_in" in session:
               return f(*args, **kwargs)
          else:
               
               return redirect(url_for("login"))

    return decorated_function

#register form
class registerform(Form):
    username = StringField("Username",validators= [validators.Length(min=4,max=15),validators.DataRequired("Please Give a Username")])
    password = PasswordField("Password",validators= [validators.Length(min=4,max=12),validators.DataRequired(message="Please Give a Password"),validators.EqualTo(fieldname = "password2",message="Passwords are different.")])
    password2 = PasswordField("Repeat Password",validators= [validators.Length(min=4,max=12),validators.DataRequired()])
    email = StringField("Email address",validators= [validators.DataRequired()])


#login form
class loginform(Form):
     username = StringField("Username",validators= [validators.DataRequired()])
     password = PasswordField("Password",validators= [validators.DataRequired()])

#login page
@app.route("/", methods = ["GET", "POST"])
def login():
     con = sqlite3.connect("pizza.db")

     cursor = con.cursor()
     form = loginform(request.form)
     if request.method == "POST":
          username = form.username.data
          password_entered = form.password.data

          query = "Select * From users where username = ?;"
          cursor.execute(query,(username,))
          result = cursor.fetchall()
          if len(result) > 0:
               real_password = str(result[0][1])
              
               if password_entered == real_password:
                    session["logged_in"] = True
                    session["username"] = username
                    session["password"] = password_entered
                    

                    return redirect(url_for("dashboard"))
                   

                    
                    
               else:
                     return render_template("main.html",form = form)
          else:
               
                return render_template("main.html",form = form)
     return render_template("main.html",form = form)

#register    
@app.route("/register",methods=["GET","POST"])
def register():
     form = registerform(request.form)
     if request.method == "POST" and form.validate():
          con = sqlite3.connect("pizza.db")
          cursor = con.cursor()
          username = form.username.data
          email = form.email.data
          password = form.password.data
          query = "INSERT INTO users (username,pass,email) VALUES (?,?,?);"
          cursor.execute(query,(username,password,email))
          con.commit()
          cursor.close()
          return redirect(url_for("login"))

     else: 
          return render_template("register.html",form=form)


@app.route("/dashboard", methods=['GET', 'POST'])
@login_required
def dashboard():
     
     con = sqlite3.connect("pizza.db")
     cursor = con.cursor()
     username = session["username"]
     query = "SELECT * from tasks where username = ? "
     data = cursor.execute(query,(username,))
    

     return render_template("dashboard.html", data = data)


@app.route("/logout")
def logout():
     session.clear()
     return redirect(url_for("login"))

#Task uploadform
class UploadForm(Form):
    taskname = StringField("Taskname",validators= [validators.Length(min=1,max=15),validators.DataRequired("Please Give a Taskname")])
    file = FileField("Upload",validators= [validators.DataRequired()])
    prediction_step = IntegerField("Prediction Step",validators= [validators.Length(min=1,max=2),validators.DataRequired("Please Give a Number")])

#deletetask
@app.route('/delete/<string:task_name>')
@login_required
def delete(task_name):
     con = sqlite3.connect("pizza.db")
     cursor = con.cursor()
     username = session["username"]
     query = ("DELETE FROM tasks WHERE username = ? and taskname = ?;")
     cursor.execute(query,(username,task_name))
     con.commit()
     return redirect(url_for("dashboard"))
#addtask
@app.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
     con = sqlite3.connect("pizza.db")
     cursor = con.cursor()
     username = session["username"]
     #fetch userid
     query = "SELECT username FROM users where username = ?"
     cursor.execute(query,(username,))
     username = cursor.fetchall()[0][0]
     
     

     if request.method == "POST":
          
          current_time = datetime.now()
          file = request.files['inputFile']
          #extension control
          if extension_control(file.filename):
               
               taskname = request.form['Taskname']
               name = taskname.replace(" ", "")
               started_date = "-"
               finished_date = "-"
               file.save(os.path.join(app.config['UPLOAD_FOLDER'], name+".xlsx"))
               base_url = (request.base_url).strip('upload')
               file_url = (base_url + os.path.join(app.config['UPLOAD_FOLDER'], name+".xlsx"))
               step = request.form['step']
              
               file_query="INSERT INTO tasks VALUES (?,?,?,?,?,?,?);"
               cursor.execute(file_query,(name,current_time,started_date,finished_date,step,file_url,username))
               con.commit()
                              
               return redirect(url_for('dashboard'))
          else:
              
               flash('File extension is not supported')
               return redirect(url_for('upload'))


     return render_template('upload.html')


#result
@app.route('/result/<string:task_name>')
def result(task_name):
     con = sqlite3.connect("pizza.db")
     cursor = con.cursor()
     query = "Select file_url from tasks where taskname = ? "
     cursor.execute(query,(task_name,))
     data = cursor.fetchall()
     data2 = data[0][0]
     data3 = pd.read_excel(data2).values
     data4 = pd.read_excel(data2)
     a = data4.CATEGORY.value_counts()
     dict_category = a.to_dict()

     values_per_category = [ [k,v] for k, v in dict_category.items() ]
     lenght_data = len(data3)
     #pizza categories
     categories = []

     for z,x,c in data3:
          if c not in categories:
               categories.append(c)
    
     
     return render_template('result.html',lenght_data = lenght_data, data3 = data3,categories = categories,task_name = task_name,values_per_category = values_per_category)
     
# def get_pizzadata(taskname):
#      con = sqlite3.connect("pizza.db")
#      cursor = con.cursor()
#      query = "Select file_url from tasks where taskname = ? "
#      cursor.execute(query,(taskname,))
#      data = cursor.fetchall()
#      data2 = data[0][0]
#      data3 = pd.read_excel(data2).values
#      #pizza categories
#      categories = []
#      for z,x,c in data3:
#           if c not in categories:
#                categories.append(c)
     

@app.route('/<string:task_name>/<string:category_name>')
def graph(task_name,category_name):
     con = sqlite3.connect("pizza.db")
     cursor = con.cursor()
     query = "Select file_url from tasks where taskname = ? "
     cursor.execute(query,(task_name,))
     data = cursor.fetchall()
     data2 = data[0][0]
     data3 = pd.read_excel(data2).values
     
     # linear regression#
     Df = pizza_list(data3,category_name)
     
     Df.set_index('time' ,inplace=True)

     # df.
     # Define explanatory variables The explanatory variables in this strategy are the moving averages for past 6 days and 12 days. 
     Df['S_2'] = Df['sales'].shift(1).rolling(window=6).mean()

     Df['S_4']= Df['sales'].shift(1).rolling(window=12).mean()


     #overcome NaN values
     Df= Df.fillna(Df.mean())

     X = Df[['S_2','S_4']]

     X.head()

     y = Df['sales']
     

     t=.4 #%40 of data is used by train data

     t = int(t*len(Df))
     # Train dataset

     X_train = X[:t]

     y_train = y[:t]
     # Test dataset

     X_test = X[t:]

     y_test = y[t:]
    

     #linear regression model
     linear = LinearRegression().fit(X_train,y_train)



     predicted_sales = linear.predict(X_test)

     predicted_sales = pd.DataFrame(predicted_sales,index=y_test.index,columns = ['sales'])
     predicted_sales.plot(figsize=(10,5))
     y_test.plot()
     plt.legend(['predicted sale','actual_sale'])
     plt.ylabel("Pizza Sales")
     plt.show()

     
     r2_score = linear.score(X[t:],y[t:])*100
     score = float("{0:.2f}".format(r2_score))
    
     return render_template("detail.html",score = score,X_train = X_train, y_train = y_train, X_test = X_test , y_test = y_test, t = t)


#date-converter for graph
def converter(s, format_list, format_output):
    for date_format in format_list:
        try:
            return datetime.strptime(s, date_format).strftime(format_output)
        except ValueError:
            continue
    return 'Nuffin'



#DataFrame of specific pizza category
def pizza_list(data,category):
     pizza_dates = []
     pizza_sales = []
     pizza_dates_eu = [] 
     for i,j,x in data:
          if x == category:
               pizza_dates.append(i)
               pizza_sales.append(j)
     
     for i in pizza_dates:
          c = str(i)
          b = c.split(" ")
          x = b[0]
          

          res = converter(x, format_list=['%d-%m-%Y', '%Y-%m-%d', '%m-%d-%Y', '%Y-%d-%m'], format_output='%m.%Y.%d')
          pizza_dates_eu.append(res[3:])              
         
     data = zip(pizza_dates_eu,pizza_sales)
     df = pd.DataFrame(data, columns = ['time', 'sales'])
    
  
    
     
   
     return df               

          



if __name__ == "__main__":
    app.run(debug=True)

